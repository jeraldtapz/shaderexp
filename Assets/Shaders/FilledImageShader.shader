﻿Shader "Unlit/FilledImageshader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_FillAmount("Fill Amount", Range(-1,1)) = 0
		_ScaleFactor("Fill Scale Factor", Float) = 5
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 localPos: NORMAL;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			half _FillAmount;
			half4 _GameObjectPos;
			half _ScaleFactor;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.localPos = v.vertex;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				if (i.localPos.x > _FillAmount * _ScaleFactor)
					col.a = 0;
				return col;
			}
			ENDCG
		}
	}
}
