﻿ Shader "Custom/StencilWindow" {
	Properties {
		_SRef("Stencil Ref", Float) = 1
		[Enum(UnityEngine.Rendering.CompareFunction)] _SComp("Stencil Compare", Float) = 8
		[Enum(UnityEngine.Rendering.StencilOp)]  _SOp("Stencil Operation", Float) = 2
	}
	SubShader {
		Tags { "Queue"="Geometry-1" }
		LOD 200
		ZWrite Off
		ColorMask 0

		Stencil
		{
			Ref[_SRef]
			Comp[_SComp]
			Pass[_SOp]
		}

		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 3.0

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			o.Albedo = fixed3(1,1,1);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
