﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnifyingGlassController : MonoBehaviour
{

    [SerializeField]
    private Transform _CameraTransform;

    private float _VerticalLookRotation = 0f;
    private Vector2 _MouseDir = Vector2.zero;


    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        _MouseDir = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        _VerticalLookRotation += _MouseDir.y;
        _VerticalLookRotation = Mathf.Clamp(_VerticalLookRotation, -60, 60);
        transform.Rotate(Vector3.up * _MouseDir.x);
        _CameraTransform.localEulerAngles = Vector3.left * _VerticalLookRotation;
    }
}
